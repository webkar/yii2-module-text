<?php
/**
 * This file is part of the Webkar project.
 *
 * (c) Webkar project <http://github.com/omidmm/>
 *
 * For the full copyright and license information, please view the LICENSE,Or visit to webkar.net 
 *
 * file that was distributed with this source code.
  * @link http://www.webkar.net/
 * @copyright Copyright (c) 2018 webkar 
 * @license http://www.webkar.net/license/
 */
namespace omidmm\text\admin\api;

use Yii;
use omidmm\admin\admin\components\API;
use omidmm\admin\admin\helpers\Data;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use omidmm\text\admin\models\Text as TextModel;
use yii\helpers\Html;

/**
 * Text module API
 * @package omidmm\text\admin\api
 *
 * @method static get(mixed $id_slug) Get text block by id or slug
 */
class Text extends API
{
    private $_texts = [];

    public function init()
    {
        parent::init();

   
    }

//    public function api_get($id_slug)
//    {
//        if(($text = $this->findText($id_slug)) === null){
//           // return $this->notFound($id_slug);
//        }
//          $result= array();
//             $resText = $text['text'];
//        return $result['text'] =(string)$resText ;
//    }
//
public function api_get($id_slug){
            if(($text = $this->findText($id_slug)) === null){
             //    return $this->notFound($id_slug);
                return '';
          }
          return  $text->text;
}
    private function findText($id_slug)
    {
      return $text  =   TextModel::find()->where( ['text_id' => $id_slug])->one();
    }

    private function notFound($id_slug)
    {
        $text = '';

        if(!Yii::$app->user->isGuest && preg_match(TextModel::$SLUG_PATTERN, $id_slug)){
            $text = Html::a(Yii::t('modules/text', 'Create text'), ['/admin/text/a/create', 'slug' => $id_slug], ['target' => '_blank']);
        }

        return $text;
    }
}