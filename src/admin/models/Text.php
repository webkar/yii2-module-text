<?php
/**
 * This file is part of the Webkar project.
 *
 * (c) Webkar project <http://github.com/omidmm/>
 *
 * For the full copyright and license information, please view the LICENSE,Or visit to webkar.net 
 *
 * file that was distributed with this source code.
  * @link http://www.webkar.net/
 * @copyright Copyright (c) 2018 webkar 
 * @license http://www.webkar.net/license/
 */
namespace omidmm\text\admin\models;

use Yii;
use omidmm\admin\admin\behaviors\CacheFlush;
use omidmm\admin\admin\behaviors\PersianSluggableBehavior;
use omidmm\language\admin\behaviors\TranslateableBehavior;
use yii\behaviors\SluggableBehavior;

class Text extends \omidmm\admin\admin\components\ActiveRecord
{
    const CACHE_KEY = 'progsoft_text';
    public static function tableName()
    {
        return '{{%texts}}';
    }

    public function rules()
    {
        return [
            ['text_id', 'number', 'integerOnly' => true],
            [['text', 'title'] ,'required'],
            [['text', 'title'],'trim'],
            ['title','string','max'=>255],
////            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('progsoft', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            [['slug', 'lang_id'], 'unique', 'targetAttribute' => ['slug','lang_id']],
            ['lang_id','default','value'=>Yii::$app->language]
        ];
    }

    public function attributeLabels()
    {
        return [
           'title' => Yii::t('progsoft', 'Title'),
            'text' => Yii::t('progsoft', 'Text'),
            'slug' => Yii::t('progsoft', 'Slug'),
        ];
    }
           

    public function behaviors()
    {
        return [
            CacheFlush::className(),
             'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true
            ],
            'translate'=>[
                'class'=>TranslateableBehavior::className(),
                 'attributes'=>['title','text'],
            ],

        ];
    }

}