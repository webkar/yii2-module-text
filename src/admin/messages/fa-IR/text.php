<?php
return [
    'Texts' => 'متن	',
    'Create text' => 'ساخت متن',
    'Edit text' => 'ویرایش متن',
    'Text created' => 'متن ساخته شد',
    'Text updated' => 'متن بزور رسانی شد',
    'Text deleted' => 'متن حذف شد'
];