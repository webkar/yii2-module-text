<?php
use yii\helpers\Url;
$this->title = Yii::t('modules/text', 'Texts');

$module = $this->context->module->id;
?>

<?= $this->render('_menu') ?>

<?php
// echo \pceuropa\forms\FormBuilder::widget([
//         'test_mode' => false
// ]);

?>

<?php if($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <?php if(IS_ROOT) : ?>
                    <th width="50">#</th>
                <?php endif; ?>
                <th><?= Yii::t('progsoft', 'Title') ?></th>
                 <th><?= Yii::t('progsoft','Lang_id') ?></th>
                <?php if(IS_ROOT) : ?>
                    <th><?= Yii::t('progsoft', 'Slug') ?></th>

                    <th width="30"></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
    <?php foreach($data->models as $item) : ?>
            <tr>
                <?php if(IS_ROOT) : ?>
                <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td><a href="<?= Url::to(['/admin/'.$module.'/a/edit', 'id' => $item->primaryKey]) ?>"><?= $item->title ?></a></td>
                <td><?= $item->lang_id ?></td>
                <?php if(IS_ROOT) : ?>
                    <td><?= $item->slug ?></td>
                    <td>
						<a href="<?= Url::to(['/admin/'.$module.'/a/delete', 'id' => $item->primaryKey]) ?>" class="btn btn-default confirm-delete" title="<?= Yii::t('progsoft', 'Delete item') ?>"><span class="glyphicon glyphicon-remove"></span></a>
					</td>
                <?php endif; ?>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination
    ]) ?>
<?php else : ?>
    <p><?= Yii::t('progsoft', 'No records found') ?></p>
<?php endif; ?>

<div>

</div>
