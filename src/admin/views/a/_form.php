<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    use yii\helpers\Url;
    use omidmm\admin\admin\widgets\Redactor;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form']
]); ?>
<?=  \omidmm\language\admin\widgets\language\LanguageFormSelector::widget(['model' => $model,'attribute' => 'lang_id'])  ?>
<?= $form->field($model,'title')->textInput() ?>
<?=
    $form->field($model, 'text')->widget(\dominus77\tinymce\TinyMce::className(), [
        'options' => [
            'rows' => 10,
            'placeholder' => true,
        ],
        'language' => (Yii::$app->language == 'fa-IR')? 'fa':Yii::$app->language,
        'clientOptions' => [
            'menubar' => true,
            'statusbar' => true,
            'theme' => 'modern',
            'skin' => 'lightgray-gradient', //charcoal, tundora, lightgray-gradient, lightgray
            'plugins' => [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak placeholder",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools codesample toc noneditable",
            ],
            'noneditable_noneditable_class' => 'fa',
            'extended_valid_elements' => 'span[class|style]',
            'toolbar1' => "undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            'toolbar2' => "print preview media | forecolor backcolor emoticons | codesample",
            'image_advtab' => true,
            'templates' => [
                [
                    'title' => 'Test template 1',
                    'content' => 'Test 1',
                ],
                [
                    'title' => 'Test template 2',
                    'content' => 'Test 2',
                ]
            ],
            'content_css' => [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css',
            ]
        ],
        'fileManager' => [
            'class' => \dominus77\tinymce\components\MihaildevElFinder::className(),
            'controller' => 'elf1',
            'title' => 'My File Manager',
            'width' => 900,
            'height' => 600,
            'resizable' => 'yes',
        ],
    ]);
?>

<?= (IS_ROOT) ? $form->field($model, 'slug') : '' ?>
<?= Html::submitButton(Yii::t('progsoft', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>

