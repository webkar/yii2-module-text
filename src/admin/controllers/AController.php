<?php
/**
 * This file is part of the Webkar project.
 *
 * (c) Webkar project <http://github.com/omidmm/>
 *
 * For the full copyright and license information, please view the LICENSE,Or visit to webkar.net 
 *
 * file that was distributed with this source code.
  * @link http://www.webkar.net/
 * @copyright Copyright (c) 2018 webkar 
 * @license http://www.webkar.net/license/
 */
namespace omidmm\text\admin\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;

use omidmm\admin\admin\components\Controller;
use omidmm\text\admin\models\Text;

class AController extends Controller
{
    public $rootActions = ['create', 'delete'];

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => Text::find(),
        ]);
        return $this->render('index', [
            'data' => $data
        ]);
    }

    public function actionCreate($slug = null)
    {
        $model = new Text;
        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if($model->save()){
                    $this->flash('success', Yii::t('modules/text', 'Text created'));
                    return $this->redirect(['/admin/'.$this->module->id]);
                }
                else{
                    $this->flash('error', Yii::t('progsoft', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        else {
            if($slug){
                $model->slug = $slug;
            }
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }


    public function actionEdit($id)
    {
        $model = Text::findOne($id);
        if($model === null){
            $this->flash('error', Yii::t('progsoft', 'Not found'));
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if($model->save()){
                    $this->flash('success', Yii::t('modules/text', 'Text updated'));
                }
                else{
                    $this->flash('error', Yii::t('progsoft', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

    public function actionDelete($id)
    {
        if(($model = Text::findOne($id))){
            $model->delete();
        } else {
            $this->error = Yii::t('progsoft', 'Not found');
        }
        return $this->formatResponse(Yii::t('modules/text', 'Text deleted'));
    }
}