<?php 
/**
 * This file is part of the Webkar project.
 *
 * (c) Webkar project <http://github.com/omidmm/>
 *
 * For the full copyright and license information, please view the LICENSE,Or visit to webkar.net 
 *
 * file that was distributed with this source code.
  * @link http://www.webkar.net/
 * @copyright Copyright (c) 2018 webkar 
 * @license http://www.webkar.net/license/
 */
namespace omidmm\text\admin\widgets\text;
#Copyright (c) 2016-2017 Omid mamandi Azar progsoft.ir
use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use omidmm\text\admin\api\Text as TextApi;
use omidmm\text\admin\models\Text as TextModels;

class TextWidgetConfig extends \yii\base\Widget {
    
	public $id_slug ;
	
	public   $render_widget = '\omidmm\text\admin\widget\TextWidget::widget([';
	public $models;	
	public $id ;
	public $json;
	public function init() {
		parent::init();
	$this->models = TextModels::find()->all();
	$json = new Json;
	$json_arr = $json->decode($this->json);
	$this->id = $json_arr['id'];

	}

	public function run() {
		if (!empty($this->models) && empty($this->id)) {
			return $this->TextBuilderRender();
		}
		if (!empty($this->id)) {
			
         if(($text = TextModels::findOne($this->id))){

          //  return $this->render_widget.'\'slug\''.'=>\''.$text->primaryKey.'\',]);';
            return json_encode(['slug'=>$text->primaryKey]);
        }else{
            return 'not found';
        }
		}
		
	}
	

	public function TextBuilderRender() {
	
		return $this->render('builder', ['models'=>$this->models]);
	}
	
   
}
    
?>
