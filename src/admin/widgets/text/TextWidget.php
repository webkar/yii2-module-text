<?php
/**
 * This file is part of the Webkar project.
 *
 * (c) Webkar project <http://github.com/omidmm/>
 *
 * For the full copyright and license information, please view the LICENSE,Or visit to webkar.net 
 *
 * file that was distributed with this source code.
  * @link http://www.webkar.net/
 * @copyright Copyright (c) 2018 webkar 
 * @license http://www.webkar.net/license/
 */
namespace omidmm\text\admin\widgets\text;
#Copyright (c) 2016-2017 Omid mamandi Azar progsoft.ir
use Yii;
use yii\helpers\Url;
use yii\helpers\Html;

use omidmm\text\admin\api\Text as TextApi;
class TextWidget extends \yii\base\Widget {
	public $slug;
	public $json ;
	public $data;
	public $text;
	public $parameters;
	public $options;
	public $binding;
	public function init() {
		parent::init();
		$this->text = (isset($this->data['text']))? $this->data['text']:self::dataBuilding($this->parameters->slug) ;
	}
   private static function dataBuilding($slug){
		return TextApi::get($slug);
   }

	public function run() {
	        		return $this->TextShow();

	}

	
	public function TextShow() {
	
		return $this->render('text-show', ['text'=>$this->text,'json'=>$this->json] );
	}
	
   
}
    
?>
