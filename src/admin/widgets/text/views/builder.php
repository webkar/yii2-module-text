<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use omidmm\admin\admin\widgets\Redactor;
use yii\helpers\ArrayHelper;
use yii\web\View;
   

$this->registerJs('
  $(document).ready(function(){
 
 
    });
    
  ', $this::POS_READY);



 

?>


    <form  id="widget_form_id" method="POST" action="<?= Url::toRoute('/admin/Widgets/widget/show-config') ?>">
        <div class="form-group">
          <label for="sel1">Select Text:</label>
          <select class="form-control" id="select_item">
          <?php foreach ($models as $key => $model): ?>
               <option value="<?= $model->text_id ?>"><?= substr($model->title,0,30) ?></option>
          <?php endforeach ?>
          </select>
        </div>
     
    </form>

<div id="output_widget"></div>

