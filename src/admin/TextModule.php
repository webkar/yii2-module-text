<?php
/**
 * This file is part of the Webkar project.
 *
 * (c) Webkar project <http://github.com/omidmm/>
 *
 * For the full copyright and license information, please view the LICENSE,Or visit to webkar.net 
 *
 * file that was distributed with this source code.
  * @link http://www.webkar.net/
 * @copyright Copyright (c) 2018 webkar 
 * @license http://www.webkar.net/license/
 */
namespace omidmm\text\admin;

class TextModule extends \omidmm\admin\admin\components\Module
{
    public static $installConfig = [
        'title' => [
            'en' => 'Text blocks',
            'ru' => 'Текстовые блоки',
        ],
        'icon' => 'font',
        'order_num' => 20,
    ];
}